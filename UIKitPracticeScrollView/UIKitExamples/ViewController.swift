//
//  ViewController.swift
//  UIKitExamples
//
//  Created by Александров Роман Витальевич on 23.02.2022.
//

import UIKit

class ViewController: UIViewController {
    
    private lazy var scrollView: UIScrollView = {
        let sv = UIScrollView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.backgroundColor = .systemCyan
        return sv
    }()
    
    private lazy var containerView: UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(scrollView)
        scrollView.addSubview(containerView)
        
        let redView = UIView()
        redView.translatesAutoresizingMaskIntoConstraints = false
        redView.backgroundColor = .systemRed
        
        let blueView = UIView()
        blueView.translatesAutoresizingMaskIntoConstraints = false
        blueView.backgroundColor = .systemBlue
        
        let orangeView = UIView()
        orangeView.translatesAutoresizingMaskIntoConstraints = false
        orangeView.backgroundColor = .systemOrange
        
        [redView, blueView, orangeView].forEach { containerView.addSubview($0) }
        
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            scrollView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            
            containerView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            containerView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            containerView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            containerView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
            containerView.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width),
            
            redView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 20),
            redView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            redView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
            redView.heightAnchor.constraint(equalToConstant: 300),

            blueView.topAnchor.constraint(equalTo: redView.bottomAnchor, constant: 20),
            blueView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            blueView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
            blueView.heightAnchor.constraint(equalToConstant: 300),
            
            orangeView.topAnchor.constraint(equalTo: blueView.bottomAnchor, constant: 20),
            orangeView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            orangeView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
            orangeView.heightAnchor.constraint(equalToConstant: 300),
            orangeView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor)
        ])
    }
}
