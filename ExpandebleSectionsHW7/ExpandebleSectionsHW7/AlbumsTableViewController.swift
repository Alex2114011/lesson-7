//
//  ViewController.swift
//  ExpandebleSectionsHW7
//
//  Created by Alex on 16.03.2022.
//

import UIKit

final class AlbumsTableViewController: UIViewController {

    //MARK: - Private property

    private var dataSource: [Genre] = [
        Genre(nameGenre: "Rock",
              album: [
                Album(nameCover: "1",
                      nameAlbum: "Sleep Through the Static",
                      ratingAlbum: 4.2),
                Album(nameCover: "2",
                      nameAlbum: "In Between Dreams",
                      ratingAlbum: 2.3),
                Album(nameCover: "3",
                      nameAlbum: "To the Sea",
                      ratingAlbum: 4.2)],
              isExpanded: false),
        Genre(nameGenre: "Pop",
              album: [
                Album(nameCover: "4",
                      nameAlbum: "So in Love",
                      ratingAlbum: 4.2),
                Album(nameCover: "5",
                      nameAlbum: "Wasting Time - Single",
                      ratingAlbum: 2.3),
                Album(nameCover: "6",
                      nameAlbum: "On and On",
                      ratingAlbum: 4.2)],
              isExpanded: false),
        Genre(nameGenre: "Holiday",
              album: [
                Album(nameCover: "7",
                      nameAlbum: "In the Morning - Single",
                      ratingAlbum: 4.2),
                Album(nameCover: "8",
                      nameAlbum: "Rudolph the Red Nosed Reindeer (Live from Late Night with Jimmy Fallon) - Single",
                      ratingAlbum: 2.3),
                Album(nameCover: "9",
                      nameAlbum: "The Captain Is Drunk - Single",
                      ratingAlbum: 4.2)],
              isExpanded: false)]

    private lazy var albumsTableView: UITableView = {
        let table = UITableView(frame: .zero, style: .grouped)
        table.dataSource = self
        table.delegate = self
        table.register(AlbumCell.self, forCellReuseIdentifier: String(describing: AlbumCell.self))
        table.register(CustomHeaderCell.self, forHeaderFooterViewReuseIdentifier:  String(describing: CustomHeaderCell.self))
        table.translatesAutoresizingMaskIntoConstraints = false
        return table
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }

    //MARK: - Private methods

    private func setupUI() {
        view.backgroundColor = albumsTableView.backgroundColor
        view.addSubview(albumsTableView)

        navigationItem.title = "Albums"
        navigationController?.navigationBar.prefersLargeTitles = true

        NSLayoutConstraint.activate([albumsTableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
                                     albumsTableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
                                     albumsTableView.trailingAnchor .constraint(equalTo: view.trailingAnchor),
                                     albumsTableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)])
    }
}

//MARK: - UITableViewDataSource

extension AlbumsTableViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataSource.count
    }


    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if dataSource[section].isExpanded {
            return dataSource[section].album.count
        } else {
            return 0
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = dataSource[indexPath.section].album[indexPath.row]
        let cell = albumsTableView.dequeueReusableCell(withIdentifier: String(describing: AlbumCell.self), for: indexPath) as! AlbumCell
        cell.configureCell(with: item)
        return cell
    }


}

//MARK: - UITableViewDelegate

extension AlbumsTableViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let item = dataSource[section]
        let header = albumsTableView.dequeueReusableHeaderFooterView(withIdentifier: String(describing: CustomHeaderCell.self)) as! CustomHeaderCell

        header.configureHeaderCell(with: item.nameGenre, section: section)

        header.buttonTapped = { [weak self] in
            guard let self = self else { return }

            var indexPaths = [IndexPath]()

            for row in self.dataSource[section].album.indices {
                let indexPath = IndexPath(row: row, section: section)
                indexPaths.append(indexPath)
            }

            let isExpanded = self.dataSource[section].isExpanded
            header.rotateImage(isExpanded)
            self.dataSource[section].isExpanded = !isExpanded

            if isExpanded {
                self.albumsTableView.deleteRows(at: indexPaths, with: .left)
            } else {
                self.albumsTableView.insertRows(at: indexPaths, with: .left)
            }
        }
        return header
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
}
