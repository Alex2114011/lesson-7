//
//  CellModel.swift
//  ExpandebleSectionsHW7
//
//  Created by Alex on 16.03.2022.
//

import Foundation

struct Genre {
    let nameGenre: String
    let album: [Album]
    var isExpanded: Bool
}

struct Album {
    let nameCover: String
    let nameAlbum: String
    let ratingAlbum: Double
}
