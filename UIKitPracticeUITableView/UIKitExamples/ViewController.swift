//
//  ViewController.swift
//  UIKitExamples
//
//  Created by Александров Роман Витальевич on 23.02.2022.
//

import UIKit

class ViewController: UIViewController {
    
    var models: [CustomCellModel] = [CustomCellModel(title: "Ckdsklfs sdkflsk slkf lskkdf",
                                                     imageString: "trash"),
                                     CustomCellModel(title: "Ckdsklfs",
                                                     imageString: "trash"),
                                     CustomCellModel(title: "Ckdsklfs sdkflsk slkf lskkdf fsdjfdsjjdfs jsdfkj jhsdkfj kj jfskjsdkfj jskdfj ksjdfk jskdsjs kjdfkjs fkjsdfj ksdjfksd js",
                                                     imageString: "trash")]
    
    private let customCellId = "customCellId"
    
    private lazy var tableView: UITableView = {
        let tv = UITableView()
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.delegate = self
        tv.dataSource = self
        tv.estimatedRowHeight = 50
        tv.rowHeight = UITableView.automaticDimension
        tv.register(CustomCell.self, forCellReuseIdentifier: customCellId)
        return tv
    }()
    
    private lazy var containerView: UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(tableView)
        
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
        ])
    }
}

// MARK: - UITableViewDataSource & UITableViewDelegate
extension ViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(
            withIdentifier: customCellId,
            for: indexPath
        ) as? CustomCell else { return UITableViewCell() }
        
        let model = models[indexPath.row]
        cell.configureCell(title: model.title, imageString: model.imageString)
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        models.count
    }
}
