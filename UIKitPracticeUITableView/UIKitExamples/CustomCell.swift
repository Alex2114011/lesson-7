//
//  CustomCell.swift
//  UIKitExamples
//
//  Created by Александров Роман Витальевич on 10.03.2022.
//

import UIKit

final class CustomCell: UITableViewCell {
    
    lazy var customImageView: UIImageView = {
        let iv = UIImageView()
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    lazy var customTitleLabel: UILabel = {
        let l = UILabel()
        l.numberOfLines = 0
        l.translatesAutoresizingMaskIntoConstraints = false
        return l
    }()
    
    lazy var button: UIButton = {
        let btn = UIButton(type: .system)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.backgroundColor = .systemBlue
        btn.setTitle("КНОПУЛЯ", for: .normal)
        btn.setTitleColor(.white, for: .normal)
        return btn
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        initCell()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureCell(title: String, imageString: String) {
        customTitleLabel.text = title
        customImageView.image = UIImage(systemName: imageString)
    }
}

// MARK: - Private methods
extension CustomCell {
    private func initCell() {
        contentView.addSubview(customImageView)
        contentView.addSubview(customTitleLabel)
        contentView.addSubview(button)

        NSLayoutConstraint.activate([
            customImageView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 20),
            customImageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            customImageView.widthAnchor.constraint(equalToConstant: 75),
            customImageView.heightAnchor.constraint(equalToConstant: 75),
            customImageView.bottomAnchor.constraint(lessThanOrEqualTo: contentView.bottomAnchor, constant: -20),
            
            customTitleLabel.topAnchor.constraint(equalTo: customImageView.topAnchor),
            customTitleLabel.leadingAnchor.constraint(equalTo: customImageView.trailingAnchor, constant: 20),
            customTitleLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),
            
            button.topAnchor.constraint(equalTo: customTitleLabel.bottomAnchor, constant: 20),
            button.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),
            button.bottomAnchor.constraint(lessThanOrEqualTo: contentView.bottomAnchor, constant: -20)
        ])
    }
}
